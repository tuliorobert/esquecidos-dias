window.onload = function() {
    document.querySelector('.preloader').style.display = 'none';
    document.querySelector('.content').style.display = 'flex';
};

uploadcare.Widget('[role=uploadcare-uploader]').onUploadComplete(function(info) {
    let imgPreview = document.getElementById('previewImagem');
    imgPreview.src = info.cdnUrl;
    imgPreview.style.display = 'block';
    imgPreview.style.width = '150px';
    imgPreview.style.height = '200px';
    imgPreview.style.border = '2px solid #000';
    imgPreview.style.borderRadius = '5px';
    imgPreview.style.marginTop = '10px';
});

function toForm() {
    const content = document.querySelector('.content');

    content.style.animation = "fadeOut 2.5s"

    setTimeout(() => {
        document.location.href = "sign-up.html";
    }, 2000);
}

document.addEventListener("DOMContentLoaded", function() {
    const redirectField = document.getElementById("redirectField");
    redirectField.value = window.location.origin + "/success.html";
});